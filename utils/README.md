# Utilities

These are scripts that help me conlang easier.

These scripts are licensed under GPLv3 license.

## Word generation

Requirements: raku

Usage:

```sh
raku genwords.raku <max syllable count> <number of word generated>
```

## Generating glyphs

Requirements: raku (JSON::Tiny), python3 (svgutils)

```sh
raku parse-syllable.raku <syllable> | python3 draw_syllable.py
```
