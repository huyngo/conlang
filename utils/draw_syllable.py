# Generate syllables from romanization
# Copyright (C) 2021 Ngô Ngọc Đức Huy
#
# This file is part of Hàësdáïga utils.
#
# Hàësdáïga utils is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Hàësdáïga utils is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Hàësdáïga utils.  If not, see <https://www.gnu.org/licenses/>.
from json import loads

import svgutils.transform as svg

glyphs = loads(input())
offset = 0
GLYPH_SIZE = ("800px", "800px")
TEXT_SIZE = (f"{len(glyphs) * 900}px", "800px")

background = svg.fromstring(f"""
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd" >
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 {len(glyphs) * 900} 800">
</svg>
""")
background.set_size(TEXT_SIZE)


def prepare_letter(name):
    glyph_svg = svg.fromfile(f'../src/haasdaiga/fonts/{name}.svg')
    glyph_svg.set_size(GLYPH_SIZE)
    glyph_root = glyph_svg.getroot()
    return glyph_root

for glyph in glyphs:
    if glyph['type'] == 'punctuation':
        name = glyph['name']
        glyph_root = prepare_letter(name)
        glyph_root.moveto(offset, 0)
        background.append([glyph_root])
    elif glyph['type'] == 'space':
        offset -= 400
    else:
        C1 = glyph['C1']
        C2 = glyph['C2']
        V1 = glyph['V1']
        V2 = glyph['V2']
        T = glyph['T']

        C1_root = prepare_letter(C1)
        if V1 == 'y' and T == 'level':
            C1_root.moveto(offset, 0)
            if C2 is not None:
                C2_root = prepare_letter(C2)
                C1_root.scale(0.5, 1)
                C2_root.scale(0.5, 1)
                C2_root.moveto(offset + 400, 0)
                background.append([C2_root])
            background.append([C1_root])
        else:
            if C2 is None:
                C1_root.scale(1, 0.5)
                C1_root.moveto(offset, 0)
                background.append([C1_root])
            else:
                C2_root = prepare_letter(C2)
                C1_root.scale(0.5, 0.5)
                C2_root.scale(0.5, 0.5)
                C1_root.moveto(offset, 0)
                C2_root.moveto(offset + 400, 0)
                background.append([C1_root, C2_root])
            if V1 != 'y':
                V1_root = prepare_letter(V1)
                if T != 'level':
                    T_root = prepare_letter(T)
                    if V2 is not None:
                        V2_root = prepare_letter(V2)
                        V1_root.scale(1/3, 0.5)
                        V2_root.scale(1/3, 0.5)
                        T_root.scale(1/3, 0.5)
                        V1_root.moveto(offset, 400)
                        V2_root.moveto(offset + 2 * 800/3, 400)
                        T_root.moveto(offset + 800/3, 400)
                        background.append([V1_root, V2_root, T_root])
                    else:
                        V1_root.scale(0.5, 0.5)
                        T_root.scale(0.5, 0.5)
                        V1_root.moveto(offset, 400)
                        T_root.moveto(offset + 400, 400)
                        background.append([V1_root, T_root])
                else:
                    if V2 is not None:
                        V2_root = prepare_letter(V2)
                        V1_root.scale(0.5, 0.5)
                        V2_root.scale(0.5, 0.5)
                        V1_root.moveto(offset, 400)
                        V2_root.moveto(offset + 400, 400)
                        background.append([V1_root, V2_root])
                    else:
                        V1_root.scale(1, 0.5)
                        V1_root.moveto(offset, 400)
                        background.append([V1_root])
            else:
                if T != 'level':
                    T_root = prepare_letter(T)
                    T_root.scale(1, 0.5)
                    T_root.moveto(offset, 400)
                    background.append([T_root])
    offset += 800

background.save('output.svg')
