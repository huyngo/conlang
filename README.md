# Conlangs

Repository for documenting my constructed languages.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img
    alt="Creative Commons License"
    src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a>

These documentations are licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][CC-BY-SA].

[CC-BY-SA]: https://creativecommons.org/licenses/by-sa/2.0/

## Builds

To build the book, you would need to install Rust and all the things coming with it.

1. Install `mdBook` crate

```
cargo install mdbook
```

2. Build it

```
mdbook build
```

3. Find the book in `book/`

Alternatively, you can use `mdbook-latex` for generating LaTeX/PDF, but for now
it has a broken dependency.
