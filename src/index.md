# Conlangs

I am a hobbyist conlanger and this is my collection of conlangs. Unfortunately,
I have not digitize my previous work in a standard way.  Either I write it on
phone note or write Word(tm) document (ew) and upload it to Google Drive (ew).
Git is more resilient, and now that I have a convenient way of syncing between
my devices without cloud, hopefully it will be the final version.

Currently, I am working on Hàäsdáïga, a synthetic language influenced by
[wǔxíng][wuxing] philosophy. I am also considering building a genderless,
isolated language after this one.

[wuxing]: https://en.wikipedia.org/wiki/Wuxing_(Chinese_philosophy)
