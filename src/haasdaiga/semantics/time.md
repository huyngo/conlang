# Time

## Months

In the Gaïdaäna calendar, there are ten months, named after the five phases
and yin-yang:

- Yang Wood
- Yin Wood
- Yang Fire
- Yin Fire
- Yang Earth
- Yin Earth
- Yang Metal
- Yin Metal
- Yang Water
- Yin Water

Colloquially, they are also called by number from 1 to 10.
