# Question

The preferred word orders are discussed in previous sections.  Verbs must be
used in interrogative mood.

One may have noticed in the Sentence order section that yes/no questions are
formed by adding a negative adverb at the end of the sentence.  Here are two
examples with *niï* and *chon*:

>
> *Have you ever been to the capital?*
>
>
> *Do you really never eat meat?*
