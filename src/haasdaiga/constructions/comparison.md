# Adjectives and comparison

## Adjectival verbs

Some verbs play the function adjectives do in other languages.  Such verbs
are called *adjectival verbs*.

In the predicative form, it is used in the same way as other verb.

> man-nom.mt.sg is_old-ind.prs.cnt  
> *The man is old.*

In the descriptive form, it is attached before the noun without any
conjugation.

> is_old-man-nom.mt.sg catch-ind.pst.cnt fish-acc.wa.sg
> The old man caught the fish

## Comparison

Comparison is done via prefixing.

### Comparative form

The comparative form has prefix **jìs**.

### Superlative form

The superlative form has prefix **haät**.
