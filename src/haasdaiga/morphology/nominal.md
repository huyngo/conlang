## Nominal

Nominal declension is based on case, gender, and number, in order.  Each
parameter is indicated by a phoneme, respectively consonant, vowel, and tone,
effectively forming a syllable.

### Case

There are five cases:

- **nom** nominative: s
- **acc** accusative: n
- **abl** ablative: t
- **dat** dative: d
- **gen** genitive: c

There is also vocative case, which is unmarked, but this is rarely used.

### Gender

There are five genders, named after five phases in wǔxíng:

- **wo** wood: a
- **fi** fire: e
- **ea** earth: i
- **mt** metal: o
- **wa** water: u
- unknown/mixed: y

### Number

There are three numbers:

- **sg** singular: level tone
- **sv** several: falling tone
- **pl** plural: rising tone
