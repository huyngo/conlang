# Morphology

This section concerns which forms exist for each category and not how to use them.
That will be covered in the section [Parts of Speech](../pos/index.md).
