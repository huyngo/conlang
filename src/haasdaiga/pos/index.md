# Parts of Speech

This section concerns the main parts of speech and how they are used.
Note that it does not discuss how they are ordered, which is the content of [Syntax].

In this section as well as [Syntax], there will be glosses, indicating how the
sentences are constructed.

They are written as quotes, in following order:

- Romanization (in bold)
- Gloss
- English translation (in italic)
