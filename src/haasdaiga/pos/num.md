# Numerals

| number | cardinal |
|--------|----------|
| 0      | zỳ       |
| 1      | jyng     |
| 2      | lèn      |
| 3      | tys      |
| 4      | ngúf     |
| 5      | ngit     |
| 6      | ghaf     |
| 7      | bet      |
| 8      | geng     |
| 9      | rìch     |
| 10     | nonj     |
| 100    | ghèp     |
| 1000   | lùng     |
| 10000  | bif      |

Constructing numbers:

- 14 = 10 + 4 : nonjngúf
- 143 = 100 + 4 × 10 + 3 : ghèpngúfnonjtys
- 2021 = 2 × 1000 + 0 × 100 + 2 × 10 + 1 : lènlùngzỳghèplènnonjjyng
- 1000000 = 100 × 10000 : ghèpbif

To construct ordinals, add -se to the number

21st: lènnonjjyng

To construct fractions, use it as a wood-gendered noun in genitive case.

a half: jyng lènca
