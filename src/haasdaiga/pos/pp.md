# Postposition

This section list an exhaustive list of postpositions.

## Location

| Hàëdáäiga | English  |
|-----------|----------|
| jos       | inside   |
| rung      | outside  |
| mèng      | in front of|
| tèf       | behind   |
| long      | between  |
| jím       | under, below|
| ngỳch     | above    |
| dún       | up       |
| lòn       | down     |
| váp       | next to  |
| chỳc      | near     |
| fá        | across, through|
| cong      | opposite of    |
| dàm       | beyond  |
| pam       | over    |

## Time

| Hàëdáäiga | English  |
|-----------|----------|
| músnech   | after    |
| mỳfgháä   | before   |
| sés       | until    |
| njongù    | during   |

## Other

| Hàëdáäiga | English |
|-----------|---------|
| chát      | with    |
| sànj      | without |
| janj      | in order to |

# Conjunction


| Hàëdáäiga | English |
|-----------|---------|
| chech     | but     |
| nòör      | and     |
