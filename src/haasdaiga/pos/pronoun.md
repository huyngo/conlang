# Pronoun

Pronouns are nominal anaphora, that is, short words that are used to refer to a
noun or a noun phrase.  Pronouns usually have nominal morphology, though not
strictly.  Pronoun is a closed class.

## Personal pronouns

There are 15 personal pronouns in total.  Dual pronouns and 1st person
inclusive pronouns are in fact formed by compounding others.

| person | singular | dual     | several | plural   |
|--------|----------|----------|---------|----------|
| 1st    | fènsy    | fènvynsỳ | jaächsỳ | rèëchsý  |
| 1st (inclusive) | | fèndynsỳ | jaächdynsỳ | rèëchdynsý |
| 2nd    | tènsy    | tèndynsỳ | néëcsỳ  | ngeënsý  |
| 3rd    | lúcsy    | lúccynsỳ | bóönsỳ  | waanjsý  |

### Reflexive

Reflexive pronouns occur as clitic.  They are formed by removing the gender
vowel and number tone from corresponding personal pronoun.

> **fènsu fèngòngàr zápmỳfo**  
> I-nom.wo.sg I-acc-look-ind.pst.cnt mirror-dat.mt.sg  
> *I was looking at myself in the mirror.*

It also works for indirect object:

> **tènsa tènfnìchá púürijinsechne**  
> he-nom.wo.sg he-dat-buy-ind.fut.prf birthday-gift-acc.fi.sg  
> *He'll just buy himself a birthday gift.*

There is a difference between reflexive and reciprocal for plural pronouns.

> **lúccynsỳ lúcnmeërá'ar**  
> you-nom.dual you-acc.sg-love-ind.prs.cnt  
> *You love yourselves*
>
> **lúccynsỳ lúccyǹmeërá'ar**  
> you-nom.dual you-acc.dual-love-ind.prs.cnt  
> *You love each other.*

Note: there is also a idiomatic use of reflexive pronouns:

> **fènsu fèncwítar**
> I-nom.wa.sg I-gen-be.ind.prs.cnt  
> *I am alone.*
>
> **fènsu fèncbochar ringyno**  
> I-nom.wa.sg I-gen-repair.ind.pst.prf vehicle-acc.mt.sg  
> *I fixed the car myself.*

## Indefinite pronouns

These are indefinite pronouns in Hàäsdáïga:

- hipsy: somewhen
- vàtsy: somewhere
- cotsy: somehow
- bansy: someone
- wón: which

Construction of other [pro-forms][pro-forms] are totally regular:

- It is interrogative when its verb is in interrogative mood.
- Its proximity is determined by a determiner prefix: *ci* (this) *la* (that)
- So are *any*, *all/every*, *other*, with *zòt*, *rýnj*, *púch*, respectively.
- Negative forms are formed by negating the "all/every" form. See [Negation].
- The equivalent relative pronouns is unchanged.

[pro-forms]: https://en.wikipedia.org/wiki/Pro-form

Examples:

TODO: add glosses

> **vungà las hipfy**  
> happen-ind.pst.prf that-nom somewhen-dat  
> *It happened at some point*.
>
> **saändasi Aäratraäce voö'ir vàtfy**  
> city-nom.ea.sg ember-gen.fi be_located_at-int.prs.cnt somewhere-dat  
> *Where is Ember city?*
>
> **wónjannane lúcfi càhánì**  
> which-name-acc.fi.sg you-dat.ea.sg give-int.pst.prf  
> *What is your name?*
>
> **vifi bansy civàtfy**  
> stand-int.prs.cnt someone-nom.sg this-somewhere-dat  
> *Is there anyone here?*
>
> **rýnjbansý càï'ar njizy lan**  
> every-someone-nom.pl can-ind.prs.cnt do that-acc
> *Everyone can do that.*
>
> **dy njéna bansa lúcna daïnè'à, connar fènsu**  
> `<sub>` woman-acc.wo.sg who.nom you-acc.wo.sg help-ind.pst.prf,
> know-ind.prs.cnt I-nom.wa.sg  
> *I know the woman who helped you.*
