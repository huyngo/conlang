# Noun

## Cases

### Nominative

The nominative is used for the subject of a sentence and for vocative.

It is also used as predicative for some verbs:

- wítzy (be)
- voïczy (become)

TODO: add examples

### Accusative

Accusative case is used for direct object.

TODO: add examples

### Ablative

Ablative case is used to express the motion away from something

TODO: add examples

### Dative

Dative case is used to express the motion to a location, or being at the
location (locative).  The meaning is determined by whether the verb takes
object or not.

TODO: add examples

### Genitive

Genitive case is used to express possession or relation.

> **cibansy wítar súngsa fènci**  
> this-nom.sg be.ind.prs.cnt book-nom.wo.sg I-gen.ea.sg  
> *This is my book.*
> 
> **cisúngsa wítar fènci**  
> this-book-nom.wo.sg be.ind.prs.cnt I-gen.ea.sg  
> *This book is mine.*

*Relations* also include relations constructed by postposition.

> **tènze vamá fèncu chát**  
> he-nom.fi.sg stay-ind.fut.prf I-gen.wa.sg with  
> *He will stay with me.*
>
> **vifar bissy wócci mèng**  
> stand-ind.prs.cnt man-nom.sg shop-gen.ea.sg (in front of)  
> *There is a man standing in front of the shop.*

Genitive is also used for family name, which is placed before the given name.

## Gender

### Individuals

Nouns' gender are arbitrarily assigned, based on the philosophy.
Generally, it is quite intuitive, for example, (tree), (seed), (wood) are wood.
However, there are some cases it is unclear, like (liver) being wood, and even
counter-intuitive, like (flower) being fire.
This is because liver is is controlled by wood in the philosophy,
and flower represents the outburst state which matches more with fire.

For people, gender is determined by their date of birth (people born in wood
months are wood).  For people who can use magic, their magic affinity is their
gender.

### Mixed groups

The gender of a group of objects with different genders is determined by the *majority*
group.  The majority group is defined as the group with:

- more than half of the group
- has more than 10 members, if any other group has more than one member.

## Number

- singular: zero or one
- several: two to ten, inclusive
- plural: more than ten
