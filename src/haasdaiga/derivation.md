# Derivational Morphology

## Nominalization

In all following transformations, where a noun has unknown gender mark *-y*,
it is replaced with an ambiguous, idiomatic gender or the  gender of a closest
noun that defines it.
In the former case, such words would be listed in the dictionary.

| category | transformation | examples |
|----------|----------------|----------|
| action   | -zy → -sy      | chopzy (think) → chopsu (the act of thinking) |
| actor    | -zy → -nersy   | rypzy (teach) → rypnersy (teacher) |
| result   | -zy → -mensy   | chopzy (think) → chopmensu (thought), ghószy (draw) → ghósmense (drawing, picture) |
| diminutive| -sy → -zìssy  | wuüpsi (dog) → wuüpzìssy (puppy), nùchsa (kid) → nùchzìssa (baby), lìmsi (human) → lìmzìssi (dwarf) |
| augmentative| -sy → gáchsy| lìmsi (human) → lìmgáchsi (giant) |

## Verbalization

| category | transformation | examples |
|----------|----------------|----------|
| reverse  | V → réc-V      | njizy (do) → récnjizy (undo)|
| do again | V → bàn-V      | chopzy (think) → bànchopzy (rethink) |

## Compounds

TBD
