# Writing system

The native writing system has not been created yet.

## Glyph structure

Each syllable is represented by a glyph with a structure as shown here:

![The structure of a glyph for a syllable](./glyph.png)

When a component in the syllable is missing, the other components expand to
fill up the gap.  Long vowel is represented as a horizontal line in the V2
position.

Because it requires more than a font to be able to write these glyphs, I cannot
create a font to represent it.  There will be a tool to generate svg/png for
generaating glyphs from a word, however.  All text samples in this book will be
romanized, from which you can generate the native scripts.

## Order

Hàäsdáïga does not have an alphabetic order.
The alphabet order is used for us to more conveniently list words.
The order for letters with tones is level, falling, and rising.

## Letters

Letters used to form glyphs are listed in the table below:

| Latin | Letter |
|-------|--------|
| a     | ![Hàësdáïga letter a](fonts/a.svg) |
| b     | ![Hàësdáïga letter b](fonts/b.svg) |
| c     | ![Hàësdáïga letter c](fonts/c.svg) |
| ch    | ![Hàësdáïga letter ch](fonts/ch.svg) |
| d     | ![Hàësdáïga letter d](fonts/d.svg) |
| e     | ![Hàësdáïga letter e](fonts/e.svg) |
| f     | ![Hàësdáïga letter f](fonts/f.svg) |
| g     | ![Hàësdáïga letter g](fonts/g.svg) |
| gh    | ![Hàësdáïga letter gh](fonts/gh.svg) |
| h     | ![Hàësdáïga letter h](fonts/h.svg) |
| i     | ![Hàësdáïga letter i](fonts/i.svg) |
| j     | ![Hàësdáïga letter j](fonts/j.svg) |
| l     | ![Hàësdáïga letter l](fonts/l.svg) |
| m     | ![Hàësdáïga letter m](fonts/m.svg) |
| n     | ![Hàësdáïga letter n](fonts/n.svg) |
| ng    | ![Hàësdáïga letter ng](fonts/ng.svg) |
| nj    | ![Hàësdáïga letter nj](fonts/nj.svg) |
| o     | ![Hàësdáïga letter o](fonts/o.svg) |
| p     | ![Hàësdáïga letter p](fonts/p.svg) |
| r     | ![Hàësdáïga letter r](fonts/r.svg) |
| s     | ![Hàësdáïga letter s](fonts/s.svg) |
| t     | ![Hàësdáïga letter t](fonts/t.svg) |
| u     | ![Hàësdáïga letter u](fonts/u.svg) |
| v     | ![Hàësdáïga letter v](fonts/v.svg) |
| w     | ![Hàësdáïga letter w](fonts/w.svg) |
| y     | (empty) |
| z     | ![Hàësdáïga letter z](fonts/z.svg) |
| long vowel | ![Hàësdáïga long vowel](fonts/long.svg) |
| rising tone | ![Hàësdáïga rising tone](fonts/rising.svg) |
| falling tone | ![Hàësdáïga falling tone](fonts/falling.svg) |
| level tone     | (empty) |
| glottal stop /ʔ/ | ![Hàësdáïga glottal stop](fonts/glottal.svg) |
| .     | ![Hàësdáïga sentence start](fonts/period.svg) |
| ,     | ![Hàësdáïga comma](fonts/comma.svg) |
| :     | ![Hàësdáïga colon](fonts/colon.svg) |
| !     | ![Hàësdáïga exclamation mark](fonts/exclam.svg) |
| ?     | ![Hàësdáïga question mark](fonts/question.svg) |
| ( or “| ![Hàësdáïga left bracket](fonts/parenleft.svg) |
| ) or ”| ![Hàësdáïga right bracket](fonts/parenright.svg) |

Punctuation usage:

- Sentence punctuation `. ! ? :` are put at the beginning of the sentence,
    rather than the end.
- Colon is considered a sentence punctuation.  It is put at the beginning of
    the quoted phrase.
- Comma is used in a similar way as in English.
- Brackets are used for both quotation and explanation. In case of quotation,
    there should be a colon preceding the phrase.

# Input for glyph generation

While this is not a part of the language, in order to generate native script
with the provided tool, one MUST follow this guideline for writing
romanization, which is not the same as the normal romanization guideline.

- If there is a tone, the schwa `y` must be explicit.  That is, never put the
    tone on a consonant.
- The second vowel should not come with the diaresis.
- Long vowel should be represented by a tilde `~` rather than duplicating
    vowels.
- Always write syllables separately.  For example `càhánzy` (to give) must
    be written as `cà hán zy`.
- The glottal stop must be explicit.  For example `aäratse` (ash) must be
    written as `'a~ rat se`.
- Punctuations should be separated from syllables.
- Spaces should be noted with a hyphen.

The output may look bad; you can blame my poor drawing skills (I did not align
some of the strokes) and the limitation of technology (the letters are
stretched and therefore the thickness is not consistent).
