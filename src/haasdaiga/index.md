# Hàäsdáïga

## In-world background

Hàäsdáïga is a dead language spoken by ancient people in Gaïdaäna.
In the day, there was only one language in the continent, shared by people from
five peoples: *Wood* farmers, *Fire* hunters, *Earth* monks, *Metal* smiths and
*Water* seafarers.  They embraced the philosophy and that reflected in their
languages.  They not only used the language to communicate, but also to define
a method with which they could control these five natural forces.

Later, as these peoples mixed up, their ability to harness these five natural
forces strengthened for some and weakened for the others.  Most notably, the
*Metal* and *Water* people lost almost all of this power, and only a minority
could still use it.  Science and trade thus became their main strength.  On the
other hand, *Fire* and *Wood* people could control it so well, they no longer
needed the language to guide their power.  *Earth* monks were the most pure
with this power, but chose to distance themselves from others' affairs.
Naturally, the language was divided into five different languages (and two
pidgins) and the old way of using natural force was forgotten.

Incidentally, *Arrow*, the son of *Blade* and *Pearl*[^1], re-invented the way.
Born as a child of a Metal mage warrior and a magicless aristocrat in a
science-oriented society, he learned to use one heritage to support the other.
He tried to reconstruct the ancient language and defined a way to use it to combine
his magic ability.  Independently, *Iceberg*[^1] also discovered it by reading
old text he was lucky to find

We are interested in the linguistic features of this language as well as its magical
structure. In this book we discuss both of them.

[^1]: These names are temporary, but they carry the meaning of their future names.
